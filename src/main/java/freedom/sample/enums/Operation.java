/**
 * 
 */
package freedom.sample.enums;

/**
 * @author charlie
 *
 */
public enum Operation {
	
	ENABLE, DISABLE, MODIFY

}
