/**
 * 
 */
package freedom.sample.enums;

/**
 * @author charlie
 *
 */
public enum RuleStatus {
	
	ENABLED, DISABLED;
	
	/**
	 * 
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		for (int i = 0; i < values().length; i++) {
			RuleStatus a = RuleStatus.values()[i];
			System.out.println(a.ordinal());
		}
	}


}
