/**
 * 
 */
package freedom.sample.enums;

/**
 * @author charlie
 *
 */
public enum ControlLogic {
	/*
	 * 是其中之一，在其中，大于，大于等于，小于，小于等于， 在min（含）与max（含）之间，在min（不含）与max（不含）之间，
	 * 在min（含）与max（不含）之间，在min（不含）与max（含）之间
	 */
	ONE_OF, IN, GT, GE, LT, LE, BTW, BTW_NOT_MIN, BTW_NOT_MAX, BTW_NOT_BOTH
	

}
