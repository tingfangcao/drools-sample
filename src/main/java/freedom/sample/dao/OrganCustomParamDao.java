/**
 * 
 */
package freedom.sample.dao;

import freedom.sample.model.OrganCustomParam;

/**
 * @author charlie
 *
 */
public interface OrganCustomParamDao extends BaseDao<OrganCustomParam> {

}
