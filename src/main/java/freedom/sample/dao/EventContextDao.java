/**
 * 
 */
package freedom.sample.dao;

import freedom.sample.model.EventContext;

/**
 * @author charlie
 *
 */
public interface EventContextDao {
	
	void save(EventContext eventContext);

}
