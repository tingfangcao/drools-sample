/**
 * 
 */
package freedom.sample.dao.impl;

import freedom.sample.dao.RuleTemplateDao;
import freedom.sample.model.RuleTemplate;

/**
 * @author charlie
 *
 */
public class RuleTemplateDaoImpl extends BaseDaoImpl<RuleTemplate> implements RuleTemplateDao {

	/* (non-Javadoc)
	 * @see freedom.sample.dao.impl.BaseDaoImpl#getEntityClass()
	 */
	@Override
	public Class<RuleTemplate> getEntityClass() {
		return RuleTemplate.class;
	}


}
