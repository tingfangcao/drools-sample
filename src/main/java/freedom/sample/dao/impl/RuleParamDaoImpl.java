/**
 * 
 */
package freedom.sample.dao.impl;

import freedom.sample.dao.RuleParamDao;
import freedom.sample.model.RuleParam;

/**
 * @author charlie
 *
 */
public class RuleParamDaoImpl extends BaseDaoImpl<RuleParam> implements RuleParamDao {

	/* (non-Javadoc)
	 * @see freedom.sample.dao.impl.BaseDaoImpl#getEntityClass()
	 */
	@Override
	public Class<RuleParam> getEntityClass() {
		return RuleParam.class;
	}


}
