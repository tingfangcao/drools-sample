/**
 * 
 */
package freedom.sample.dao.impl;

import freedom.sample.dao.OrganRuleDao;
import freedom.sample.model.OrganRule;

/**
 * @author charlie
 *
 */
public class OrganRuleDaoImpl extends BaseDaoImpl<OrganRule> implements OrganRuleDao {

	/* (non-Javadoc)
	 * @see freedom.sample.dao.impl.BaseDaoImpl#getEntityClass()
	 */
	@Override
	public Class<OrganRule> getEntityClass() {
		return OrganRule.class;
	}


}
