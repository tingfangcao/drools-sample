/**
 * 
 */
package freedom.sample.dao.impl;

import freedom.sample.dao.OrganCustomParamDao;
import freedom.sample.model.OrganCustomParam;

/**
 * @author charlie
 *
 */
public class OrganCustomParamDaoImpl extends BaseDaoImpl<OrganCustomParam> implements OrganCustomParamDao {

	/* (non-Javadoc)
	 * @see freedom.sample.dao.impl.BaseDaoImpl#getEntityClass()
	 */
	@Override
	public Class<OrganCustomParam> getEntityClass() {
		return OrganCustomParam.class;
	}


}
