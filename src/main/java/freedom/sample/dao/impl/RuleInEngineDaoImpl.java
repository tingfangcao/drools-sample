/**
 * 
 */
package freedom.sample.dao.impl;

import freedom.sample.dao.RuleInEngineDao;
import freedom.sample.model.RuleInEngine;

/**
 * @author charlie
 *
 */
public class RuleInEngineDaoImpl extends BaseDaoImpl<RuleInEngine> implements RuleInEngineDao  {

	/* (non-Javadoc)
	 * @see freedom.sample.dao.impl.BaseDaoImpl#getEntityClass()
	 */
	@Override
	public Class<RuleInEngine> getEntityClass() {
		return RuleInEngine.class;
	}


}
