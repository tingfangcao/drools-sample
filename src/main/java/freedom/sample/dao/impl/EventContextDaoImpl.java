/**
 * 
 */
package freedom.sample.dao.impl;

import freedom.sample.dao.EventContextDao;
import freedom.sample.model.EventContext;

/**
 * @author charlie
 *
 */
public class EventContextDaoImpl extends BaseDaoImpl<EventContext> implements EventContextDao {

	/* (non-Javadoc)
	 * @see freedom.sample.dao.impl.BaseDaoImpl#getEntityClass()
	 */
	@Override
	public Class<EventContext> getEntityClass() {
		return EventContext.class;
	}


}
