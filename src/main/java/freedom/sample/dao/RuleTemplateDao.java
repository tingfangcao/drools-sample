/**
 * 
 */
package freedom.sample.dao;

import freedom.sample.model.RuleTemplate;

/**
 * @author charlie
 *
 */
public interface RuleTemplateDao extends BaseDao<RuleTemplate> {

}
