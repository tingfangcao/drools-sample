/**
 * 
 */
package freedom.sample.dao;

import freedom.sample.model.OrganRule;

/**
 * @author charlie
 *
 */
public interface OrganRuleDao extends BaseDao<OrganRule> {

}
