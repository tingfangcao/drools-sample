/**
 * 
 */
package freedom.sample.dao;

import freedom.sample.model.RuleInEngine;

/**
 * @author charlie
 *
 */
public interface RuleInEngineDao extends BaseDao<RuleInEngine> {

}
