/**
 * 
 */
package freedom.sample.dao;

import freedom.sample.model.RuleParam;

/**
 * @author charlie
 *
 */
public interface RuleParamDao extends BaseDao<RuleParam> {

}
