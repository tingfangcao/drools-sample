/**
 * 
 */
package freedom.sample.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author charlie
 *
 */
@Entity
@Table(name = "MARKET_ACTIVITY")
public class MarketActivity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name="CUST_ID", length=20)
	private String custId;
	@Column(name="ORGAN_NO", length=6)
	private String organNo;
	@Column(name="EVENT_CONTEXT_ID")
	private Long eventContextId;
	
	@Transient
	private List<MarketChannel> channels;
	
	/**
	 * @return the custId
	 */
	public String getCustId() {
		return custId;
	}
	/**
	 * @param custId the custId to set
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	/**
	 * @return the eventContextId
	 */
	public Long getEventContextId() {
		return eventContextId;
	}
	/**
	 * @param eventContextId the eventContextId to set
	 */
	public void setEventContextId(Long eventContextId) {
		this.eventContextId = eventContextId;
	}
	/**
	 * @return the channels
	 */
	public List<MarketChannel> getChannels() {
		return channels;
	}
	/**
	 * @param channels the channels to set
	 */
	public void setChannels(List<MarketChannel> channels) {
		this.channels = channels;
	}
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the organNo
	 */
	public String getOrganNo() {
		return organNo;
	}
	/**
	 * @param organNo the organNo to set
	 */
	public void setOrganNo(String organNo) {
		this.organNo = organNo;
	}
	

}
