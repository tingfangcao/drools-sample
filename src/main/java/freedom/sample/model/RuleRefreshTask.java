/**
 * 
 */
package freedom.sample.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import freedom.sample.enums.Operation;

/**
 * @author charlie
 *
 */
@Entity
@Table(name = "RULE_REFRESH_TASK", uniqueConstraints = @UniqueConstraint(columnNames = { "ORGAN_NO", "RULE_TEMPLATE_ID"}))
public class RuleRefreshTask implements Serializable{
	private static final long serialVersionUID = 1L;
	/*
	 * ID 机构号 事件代码 版本号 刷新原因 计划执行时间 备注 创建时间 创建人 修改时间 修改人
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name="ORGAN_NO", length=6)
	private String organNo;
	@Column(name="RULE_TEMPLATE_ID")
	private Long ruleTemplateId;
	private Operation operation;
	@Column(name="PLAN_BEGIN")
	private Date planBegin;
	@Column(length=50)
	private String remark;
	@Column(name="CREATE_USER", length=7)
	private String createUser;
	@Column(name="CREATE_TIME")
	private Date createTime;
	@Column(name="UPDATE_USER", length=7)
	private String updateUser;
	@Column(name="UPDATE_TIME")
	private Date updateTime;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the organNo
	 */
	public String getOrganNo() {
		return organNo;
	}

	/**
	 * @param organNo
	 *            the organNo to set
	 */
	public void setOrganNo(String organNo) {
		this.organNo = organNo;
	}

	/**
	 * @return the operation
	 */
	public Operation getOperation() {
		return operation;
	}

	/**
	 * @param operation
	 *            the operation to set
	 */
	public void setOperation(Operation operation) {
		this.operation = operation;
	}

	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @param remark
	 *            the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @return the createUser
	 */
	public String getCreateUser() {
		return createUser;
	}

	/**
	 * @param createUser
	 *            the createUser to set
	 */
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	/**
	 * @return the createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @param createTime
	 *            the createTime to set
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return the updateUser
	 */
	public String getUpdateUser() {
		return updateUser;
	}

	/**
	 * @param updateUser
	 *            the updateUser to set
	 */
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	/**
	 * @return the updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	/**
	 * @param updateTime
	 *            the updateTime to set
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return the planBegin
	 */
	public Date getPlanBegin() {
		return planBegin;
	}

	/**
	 * @param planBegin the planBegin to set
	 */
	public void setPlanBegin(Date planBegin) {
		this.planBegin = planBegin;
	}

	/**
	 * @return the ruleTemplateId
	 */
	public Long getRuleTemplateId() {
		return ruleTemplateId;
	}

	/**
	 * @param ruleTemplateId the ruleTemplateId to set
	 */
	public void setRuleTemplateId(Long ruleTemplateId) {
		this.ruleTemplateId = ruleTemplateId;
	}

}
