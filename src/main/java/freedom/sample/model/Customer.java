/**
 * 
 */
package freedom.sample.model;

import java.io.Serializable;

/**
 * @author charlie
 *
 */
public class Customer implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String custId;//客户内码
	private String custOrgan;//客户法人机构
	private String custType;//客户类型
	
	public Customer() {
		super();
	}
	/**
	 * @param custId
	 * @param custOrgan
	 * @param custType
	 */
	public Customer(String custId, String custOrgan, String custType) {
		super();
		this.custId = custId;
		this.custOrgan = custOrgan;
		this.custType = custType;
	}
	/**
	 * @return the custId
	 */
	public String getCustId() {
		return custId;
	}
	/**
	 * @param custId the custId to set
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	/**
	 * @return the custOrgan
	 */
	public String getCustOrgan() {
		return custOrgan;
	}
	/**
	 * @param custOrgan the custOrgan to set
	 */
	public void setCustOrgan(String custOrgan) {
		this.custOrgan = custOrgan;
	}
	/**
	 * @return the custType
	 */
	public String getCustType() {
		return custType;
	}
	/**
	 * @param custType the custType to set
	 */
	public void setCustType(String custType) {
		this.custType = custType;
	}
	
	

}
