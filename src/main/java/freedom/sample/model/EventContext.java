/**
 * 
 */
package freedom.sample.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author charlie
 *
 */
@Entity
@Table(name = "EVENT_CONTEXT")
public class EventContext implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name="CONTEXT_CLAZZ", length=200)
	private String contextClazz;
	@Column(length=500)
	private String context;
	
	/**
	 * 
	 */
	public EventContext() {
		super();
	}
	
	/**
	 * @param contextClazz
	 * @param context
	 */
	public EventContext(String contextClazz, String context) {
		super();
		this.contextClazz = contextClazz;
		this.context = context;
	}
	/**
	 * @return the contextClazz
	 */
	public String getContextClazz() {
		return contextClazz;
	}
	/**
	 * @param contextClazz the contextClazz to set
	 */
	public void setContextClazz(String contextClazz) {
		this.contextClazz = contextClazz;
	}
	/**
	 * @return the context
	 */
	public String getContext() {
		return context;
	}
	/**
	 * @param context the context to set
	 */
	public void setContext(String context) {
		this.context = context;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EventContext [id=" + id + ", contextClazz=" + contextClazz + ", context=" + context + "]";
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	

}
