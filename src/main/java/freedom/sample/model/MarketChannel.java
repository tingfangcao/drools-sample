/**
 * 
 */
package freedom.sample.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import freedom.sample.service.DeliverService.DeliverChannel;

/**
 * @author charlie
 *
 */
@Entity
@Table(name = "MARKET_CHANNEL")
public class MarketChannel implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name="RULE_TEMPLATE_ID")
	private Long ruleTemplateId;
	@Column(name="ORGAN_NO", length=6)
	private String organNo;
	private DeliverChannel channel;
	@Column(name="CHANNEL_MESSAGE", length=200)
	private String channelMessage;
	@Column(length=50)
	private String description;
	
	/**
	 * 
	 */
	public MarketChannel() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param channel
	 * @param channelMessage
	 */
	public MarketChannel(DeliverChannel channel, String channelMessage) {
		super();
		this.channel = channel;
		this.channelMessage = channelMessage;
	}
	/**
	 * @return the channel
	 */
	public DeliverChannel getChannel() {
		return channel;
	}
	/**
	 * @param channel the channel to set
	 */
	public void setChannel(DeliverChannel channel) {
		this.channel = channel;
	}
	/**
	 * @return the channelMessage
	 */
	public String getChannelMessage() {
		return channelMessage;
	}
	/**
	 * @param channelMessage the channelMessage to set
	 */
	public void setChannelMessage(String channelMessage) {
		this.channelMessage = channelMessage;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MarketChannel [channel=" + channel + ", channelMessage=" + channelMessage + "]";
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the ruleTemplateId
	 */
	public Long getRuleTemplateId() {
		return ruleTemplateId;
	}

	/**
	 * @param ruleTemplateId the ruleTemplateId to set
	 */
	public void setRuleTemplateId(Long ruleTemplateId) {
		this.ruleTemplateId = ruleTemplateId;
	}

	/**
	 * @return the organNo
	 */
	public String getOrganNo() {
		return organNo;
	}

	/**
	 * @param organNo the organNo to set
	 */
	public void setOrganNo(String organNo) {
		this.organNo = organNo;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	

}
