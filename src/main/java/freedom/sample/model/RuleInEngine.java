/**
 * 
 */
package freedom.sample.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import freedom.sample.enums.Operation;

/**
 * @author charlie
 *
 */
@Entity
@Table(name = "RULE_IN_ENGINE", uniqueConstraints = @UniqueConstraint(columnNames = { "CLUSTER_NODE", "ORGAN_NO",
		"RULE_TEMPLATE_ID" }))
public class RuleInEngine implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/*
	 * ID 节点号 机构号 事件代码  规则文本 备注 创建时间 修改时间
	 * 
	 */

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name="CLUSTER_NODE", length=10)
	private String clusterNode;
	@Column(name="ORGAN_NO", length=6)
	private String organNo;
	@Column(name="RULE_TEMPLATE_ID")
	private Long ruleTemplateId;
	@Column(name="RULE_TEXT", length=500)
	private String ruleText;
	@Column(length=50)
	private String remark;
	private Operation operation;
	@Column(name="CREATE_TIME")
	private Date createTime;
	@Column(name="UPDATE_TIME")
	private Date updateTime;
	
	@Transient
	private RuleTemplate ruleTemplate;
	@Transient
	private List<OrganCustomParam> params;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the clusterNode
	 */
	public String getClusterNode() {
		return clusterNode;
	}

	/**
	 * @param clusterNode
	 *            the clusterNode to set
	 */
	public void setClusterNode(String clusterNode) {
		this.clusterNode = clusterNode;
	}

	/**
	 * @return the organNo
	 */
	public String getOrganNo() {
		return organNo;
	}

	/**
	 * @param organNo
	 *            the organNo to set
	 */
	public void setOrganNo(String organNo) {
		this.organNo = organNo;
	}

	/**
	 * @return the ruleText
	 */
	public String getRuleText() {
		return ruleText;
	}

	/**
	 * @param ruleText
	 *            the ruleText to set
	 */
	public void setRuleText(String ruleText) {
		this.ruleText = ruleText;
	}

	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @param remark
	 *            the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @return the createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @param createTime
	 *            the createTime to set
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return the updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	/**
	 * @param updateTime
	 *            the updateTime to set
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return the ruleTemplateId
	 */
	public Long getRuleTemplateId() {
		return ruleTemplateId;
	}

	/**
	 * @param ruleTemplateId the ruleTemplateId to set
	 */
	public void setRuleTemplateId(Long ruleTemplateId) {
		this.ruleTemplateId = ruleTemplateId;
	}

	/**
	 * @return the ruleTemplate
	 */
	public RuleTemplate getRuleTemplate() {
		return ruleTemplate;
	}

	/**
	 * @param ruleTemplate the ruleTemplate to set
	 */
	public void setRuleTemplate(RuleTemplate ruleTemplate) {
		this.ruleTemplate = ruleTemplate;
	}

	/**
	 * @return the params
	 */
	public List<OrganCustomParam> getParams() {
		if(null == this.params)
			this.params = new ArrayList<OrganCustomParam>();
		return params;
	}

	/**
	 * @param params the params to set
	 */
	public void setParams(List<OrganCustomParam> params) {
		this.params = params;
	}

	/**
	 * @return the operation
	 */
	public Operation getOperation() {
		return operation;
	}

	/**
	 * @param operation the operation to set
	 */
	public void setOperation(Operation operation) {
		this.operation = operation;
	}

}
