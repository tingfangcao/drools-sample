/**
 * 
 */
package freedom.sample.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.drools.core.util.ArrayUtils;

import freedom.sample.enums.ControlLogic;
import freedom.sample.enums.YesOrNo;
import freedom.sample.exception.UnsupportedException;

/**
 * @author charlie
 *
 */
@Entity
@Table(name = "RULE_PARAM", uniqueConstraints = @UniqueConstraint(columnNames = { "RULE_TEMPLATE_ID", "CODE" }))
public class RuleParam implements Serializable, Comparable<RuleParam>{
	/*
	 * ID 事件代码 版本号 参数代码 参数类型（java) 默认值 是否可编辑 在规则中的位置 值是否带分隔符 值是否需要翻译 控制逻辑 参考值1
	 * 参考值1是否带分隔符 参考值2 参考值2是否带分隔符 备注 创建时间 创建人 修改时间 修改人
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/*
	 * 支持的数据类型
	 * String String[] Integer BigDecimal
	 */
	public static final String[] SUPPORTED_JAVA_TYPES = { String.class.getSimpleName(), String[].class.getSimpleName(),
			Integer.class.getSimpleName(), BigDecimal.class.getSimpleName() };
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name="RULE_TEMPLATE_ID")
	private Long ruleTemplateId;
	private Integer code;
	@Column(name="JAVA_TYPE", length=20)
	private String javaType;
	@Column(name="DEFAULT_VALUE", length=50)
	private String defaultValue;
	private YesOrNo editable;
	private Integer position;
	@Column(name="WITH_SEPARATOR")
	private YesOrNo withSeparator;
	@Column(name="TO_TRANSLATE")
	private YesOrNo toTranslate;
	@Column(name="CONTROL_LOGIC")
	private ControlLogic controlLogic;
	@Column(name="REF_VALUE_1", length=50)
	private String refValue1;
	@Column(name="REF_VALUE_1_WITH_SEPARATOR")
	private YesOrNo refValue1WithSeparator;
	@Column(name="REF_VALUE_2", length=50)
	private String refValue2;
	@Column(name="REF_VALUE_2_WITH_SEPARATOR")
	private YesOrNo refValue2WithSeparator;
	@Column(length=50)
	private String remark;
	@Column(name="CREATE_USER", length=7)
	private String createUser;
	@Column(name="CREATE_TIME")
	private Date createTime;
	@Column(name="UPDATE_USER", length=7)
	private String updateUser;
	@Column(name="UPDATE_TIME")
	private Date updateTime;
	
	public RuleParam() {
		super();
	}
	
	/**
	 * @param tplCode
	 * @param version
	 * @param code
	 * @param javaType
	 * @param defaultValue
	 * @param position
	 */
	public RuleParam(Integer code, String javaType, String defaultValue,
			Integer position) {
		super();
		this.code = code;
		this.javaType = javaType;
		this.defaultValue = defaultValue;
		this.position = position;
	}
	
	public RuleParam validateJavaType() throws UnsupportedException{
		if(ArrayUtils.indexOf(SUPPORTED_JAVA_TYPES, this.javaType) < 0)
			throw new UnsupportedException();
		return this;
	}
	
	public RuleParam validateControlLogic() throws UnsupportedException{
		this.validateControlLogic(this.defaultValue);
		return this;
	}
	
	public RuleParam validateControlLogic(final String customValue) throws UnsupportedException{
		//TODO
		
		return this;
	}
	
	public Object convert(final String finalValue) {
		Object val = null;
		if (finalValue != null)
			switch (javaType) {
			case "String":
				val = finalValue;
				break;
			case "Integer":
				val = Integer.valueOf(finalValue);
				break;
			case "BigDecimal":
				val = new BigDecimal(finalValue);
				break;
			case "String[]":
				val = finalValue.split(",");
				break;
	
			default:
				break;
			}
		return val;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	/**
	 * @return the javaType
	 */
	public String getJavaType() {
		return javaType;
	}
	/**
	 * @param javaType the javaType to set
	 */
	public void setJavaType(String javaType) {
		this.javaType = javaType;
	}
	/**
	 * @return the defaultValue
	 */
	public String getDefaultValue() {
		return defaultValue;
	}
	/**
	 * @param defaultValue the defaultValue to set
	 */
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	/**
	 * @return the editable
	 */
	public YesOrNo getEditable() {
		return editable;
	}
	/**
	 * @param editable the editable to set
	 */
	public void setEditable(YesOrNo editable) {
		this.editable = editable;
	}
	/**
	 * @return the withSeparator
	 */
	public YesOrNo getWithSeparator() {
		return withSeparator;
	}
	/**
	 * @param withSeparator the withSeparator to set
	 */
	public void setWithSeparator(YesOrNo withSeparator) {
		this.withSeparator = withSeparator;
	}
	/**
	 * @return the toTranslate
	 */
	public YesOrNo getToTranslate() {
		return toTranslate;
	}
	/**
	 * @param toTranslate the toTranslate to set
	 */
	public void setToTranslate(YesOrNo toTranslate) {
		this.toTranslate = toTranslate;
	}
	/**
	 * @return the controlLogic
	 */
	public ControlLogic getControlLogic() {
		return controlLogic;
	}
	/**
	 * @param controlLogic the controlLogic to set
	 */
	public void setControlLogic(ControlLogic controlLogic) {
		this.controlLogic = controlLogic;
	}
	/**
	 * @return the refValue1
	 */
	public String getRefValue1() {
		return refValue1;
	}
	/**
	 * @param refValue1 the refValue1 to set
	 */
	public void setRefValue1(String refValue1) {
		this.refValue1 = refValue1;
	}
	/**
	 * @return the refValue1WithSeparator
	 */
	public YesOrNo getRefValue1WithSeparator() {
		return refValue1WithSeparator;
	}
	/**
	 * @param refValue1WithSeparator the refValue1WithSeparator to set
	 */
	public void setRefValue1WithSeparator(YesOrNo refValue1WithSeparator) {
		this.refValue1WithSeparator = refValue1WithSeparator;
	}
	/**
	 * @return the refValue2
	 */
	public String getRefValue2() {
		return refValue2;
	}
	/**
	 * @param refValue2 the refValue2 to set
	 */
	public void setRefValue2(String refValue2) {
		this.refValue2 = refValue2;
	}
	/**
	 * @return the refValue2WithSeparator
	 */
	public YesOrNo getRefValue2WithSeparator() {
		return refValue2WithSeparator;
	}
	/**
	 * @param refValue2WithSeparator the refValue2WithSeparator to set
	 */
	public void setRefValue2WithSeparator(YesOrNo refValue2WithSeparator) {
		this.refValue2WithSeparator = refValue2WithSeparator;
	}
	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * @return the createUser
	 */
	public String getCreateUser() {
		return createUser;
	}
	/**
	 * @param createUser the createUser to set
	 */
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	/**
	 * @return the createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * @param createTime the createTime to set
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * @return the updateUser
	 */
	public String getUpdateUser() {
		return updateUser;
	}
	/**
	 * @param updateUser the updateUser to set
	 */
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	/**
	 * @return the updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	/**
	 * @param updateTime the updateTime to set
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(RuleParam o) {
		return this.position - o.position;
	}

	/**
	 * @return the ruleTemplateId
	 */
	public Long getRuleTemplateId() {
		return ruleTemplateId;
	}

	/**
	 * @param ruleTemplateId the ruleTemplateId to set
	 */
	public void setRuleTemplateId(Long ruleTemplateId) {
		this.ruleTemplateId = ruleTemplateId;
	}

	/**
	 * @return the position
	 */
	public Integer getPosition() {
		return position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(Integer position) {
		this.position = position;
	}
	
	
}
