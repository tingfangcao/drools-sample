/**
 * 
 */
package freedom.sample.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * @author charlie
 *
 */
@Entity
@Table(name = "TRANSLATE_RESOURCE", uniqueConstraints = @UniqueConstraint(columnNames = { "CATEGORY", "RESOURCE_KEY" }))
public class TranslateResource implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(length=50)
	private String category;
	@Column(name="RESOURCE_KEY", length=50)
	private String resourceKey;
	@Column(name="RESOURCE_VALUE", length=100)
	private String resourceValue;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the resourceKey
	 */
	public String getResourceKey() {
		return resourceKey;
	}

	/**
	 * @param resourceKey the resourceKey to set
	 */
	public void setResourceKey(String resourceKey) {
		this.resourceKey = resourceKey;
	}

	/**
	 * @return the resourceValue
	 */
	public String getResourceValue() {
		return resourceValue;
	}

	/**
	 * @param resourceValue the resourceValue to set
	 */
	public void setResourceValue(String resourceValue) {
		this.resourceValue = resourceValue;
	}

}
