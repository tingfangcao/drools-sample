/**
 * 
 */
package freedom.sample.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import freedom.sample.enums.TaskStatus;

/**
 * @author charlie
 *
 */
@Entity
@Table(name = "TASK_EXECUTION", uniqueConstraints = @UniqueConstraint(columnNames = { "TASK_ID", "CLUSTER_NODE" }))
public class TaskExecution implements Serializable{
	private static final long serialVersionUID = 1L;
	/*
	 * ID 任务ID 节点号 状态 开始执行时间 执行完成时间 备注
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name="TASK_ID")
	private Long taskId;
	@Column(name="CLUSTER_NODE", length=20)
	private String clusterNode;
	private TaskStatus status;
	private Date begin;
	private Date end;
	@Column(length=50)
	private String remark;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the taskId
	 */
	public Long getTaskId() {
		return taskId;
	}

	/**
	 * @param taskId
	 *            the taskId to set
	 */
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	/**
	 * @return the clusterNode
	 */
	public String getClusterNode() {
		return clusterNode;
	}

	/**
	 * @param clusterNode
	 *            the clusterNode to set
	 */
	public void setClusterNode(String clusterNode) {
		this.clusterNode = clusterNode;
	}

	/**
	 * @return the status
	 */
	public TaskStatus getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(TaskStatus status) {
		this.status = status;
	}

	/**
	 * @return the begin
	 */
	public Date getBegin() {
		return begin;
	}

	/**
	 * @param begin
	 *            the begin to set
	 */
	public void setBegin(Date begin) {
		this.begin = begin;
	}

	/**
	 * @return the end
	 */
	public Date getEnd() {
		return end;
	}

	/**
	 * @param end
	 *            the end to set
	 */
	public void setEnd(Date end) {
		this.end = end;
	}

	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @param remark
	 *            the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

}
