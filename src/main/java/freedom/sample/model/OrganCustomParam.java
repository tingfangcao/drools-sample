/**
 * 
 */
package freedom.sample.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import freedom.sample.exception.UnsupportedException;

/**
 * @author charlie
 *
 */
@Entity
@Table(name = "Organ_Custom_Param", uniqueConstraints = @UniqueConstraint(columnNames = { "ORGAN_NO",
		"RULE_PARAM_ID" }))
public class OrganCustomParam implements Serializable, Comparable<OrganCustomParam>{
	private static final long serialVersionUID = 1L;
	/*
	 * ID 机构号 参数ID 调整后的值 备注 创建时间 创建人 修改时间 修改人
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name="ORGAN_NO", length=6)
	private String organNo;
	@Column(name="RULE_PARAM_ID")
	private Long ruleParamId;
	@Column(name="RULE_TEMPLATE_ID")
	private Long ruleTemplateId;
	@Column(name="CUSTOM_VALUE", length=50)
	private String customValue;
	@Column(length=50)
	private String remark;
	@Column(name="CREATE_USER", length=7)
	private String createUser;
	@Column(name="CREATE_TIME")
	private Date createTime;
	@Column(name="UPDATE_USER", length=7)
	private String updateUser;
	@Column(name="UPDATE_TIME")
	private Date updateTime;
	
	@Transient
	private RuleParam ruleParam;
	@Transient
	private String finalValue;
	@Transient
	private Object convertedValue;
	
	public OrganCustomParam() {
		super();
	}
	
	/**
	 * @param organNo
	 * @param ruleParamId
	 * @param customValue
	 * @param ruleParam
	 */
	public OrganCustomParam(String organNo, Long ruleParamId, RuleParam ruleParam) {
		super();
		this.organNo = organNo;
		this.ruleParamId = ruleParamId;
		this.customValue = ruleParam.getDefaultValue();
		this.ruleParam = ruleParam;
	}
	
	public OrganCustomParam build() throws UnsupportedException {
		this.finalValue = customValue != null ? customValue : ruleParam.getDefaultValue();
		this.convertedValue = ruleParam.validateJavaType().validateControlLogic().validateControlLogic(customValue).convert(finalValue);
		return this;
	}
	
	public boolean isDifferentFromDefault() {
		return !this.ruleParam.getDefaultValue().equals(this.customValue);
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(OrganCustomParam o) {
		return this.ruleParam.compareTo(o.ruleParam);
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the organNo
	 */
	public String getOrganNo() {
		return organNo;
	}

	/**
	 * @param organNo
	 *            the organNo to set
	 */
	public void setOrganNo(String organNo) {
		this.organNo = organNo;
	}

	/**
	 * @return the ruleParamId
	 */
	public Long getRuleParamId() {
		return ruleParamId;
	}

	/**
	 * @param ruleParamId
	 *            the ruleParamId to set
	 */
	public void setRuleParamId(Long ruleParamId) {
		this.ruleParamId = ruleParamId;
	}

	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @param remark
	 *            the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @return the createUser
	 */
	public String getCreateUser() {
		return createUser;
	}

	/**
	 * @param createUser
	 *            the createUser to set
	 */
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	/**
	 * @return the createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @param createTime
	 *            the createTime to set
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return the updateUser
	 */
	public String getUpdateUser() {
		return updateUser;
	}

	/**
	 * @param updateUser
	 *            the updateUser to set
	 */
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	/**
	 * @return the updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	/**
	 * @param updateTime
	 *            the updateTime to set
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return the customValue
	 */
	public String getCustomValue() {
		return customValue;
	}

	/**
	 * @param customValue the customValue to set
	 */
	public void setCustomValue(String customValue) {
		this.customValue = customValue;
	}

	/**
	 * @return the ruleParam
	 */
	public RuleParam getRuleParam() {
		return ruleParam;
	}

	/**
	 * @param ruleParam the ruleParam to set
	 */
	public void setRuleParam(RuleParam ruleParam) {
		this.ruleParam = ruleParam;
	}

	/**
	 * @return the finalValue
	 */
	public String getFinalValue() {
		return finalValue;
	}

	/**
	 * @param finalValue the finalValue to set
	 */
	public void setFinalValue(String finalValue) {
		this.finalValue = finalValue;
	}

	/**
	 * @return the convertedValue
	 */
	public Object getConvertedValue() {
		return convertedValue;
	}

	/**
	 * @param convertedValue the convertedValue to set
	 */
	public void setConvertedValue(Object convertedValue) {
		this.convertedValue = convertedValue;
	}

	/**
	 * @return the ruleTemplateId
	 */
	public Long getRuleTemplateId() {
		return ruleTemplateId;
	}

	/**
	 * @param ruleTemplateId the ruleTemplateId to set
	 */
	public void setRuleTemplateId(Long ruleTemplateId) {
		this.ruleTemplateId = ruleTemplateId;
	}


}
