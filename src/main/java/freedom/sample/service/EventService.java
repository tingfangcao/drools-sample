/**
 * 
 */
package freedom.sample.service;

import freedom.sample.model.EventContext;
import freedom.sample.model.MarketActivity;

/**
 * @author charlie
 *
 */
public interface EventService {

	/**
	 * @param eventContext
	 * @return
	 */
	EventContext save(EventContext eventContext);

	/**
	 * @param marketingActivity
	 */
	void save(MarketActivity marketingActivity);

}
