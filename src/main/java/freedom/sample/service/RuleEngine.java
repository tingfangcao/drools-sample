/**
 * 
 */
package freedom.sample.service;

import com.fasterxml.jackson.core.JsonProcessingException;

import freedom.sample.event.PerfectExecutable;
import freedom.sample.exception.UnsupportedException;

/**
 * @author charlie
 *
 */
public interface RuleEngine {
	
	/**
	 * 应用启动时初始化
	 * @throws UnsupportedException 
	 */
	void init() throws UnsupportedException;
	
	/**
	 * 每天定时刷新
	 * @throws UnsupportedException 
	 */
	void refresh() throws UnsupportedException;

	void execute(PerfectExecutable event) throws JsonProcessingException;
}
