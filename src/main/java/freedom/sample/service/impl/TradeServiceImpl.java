/**
 * 
 */
package freedom.sample.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;

import freedom.sample.event.PMS100Event;
import freedom.sample.model.Customer;
import freedom.sample.service.DeliverService;
import freedom.sample.service.EventService;
import freedom.sample.service.RuleEngine;
import freedom.sample.service.TradeService;

/**
 * @author charlie
 *
 */
public class TradeServiceImpl implements TradeService {
	
	private RuleEngine ruleEngine;
	private EventService eventService;
	private DeliverService deliverService;

	/* (non-Javadoc)
	 * @see freedom.sample.TradeService#trade(freedom.sample.Customer)
	 */
	@Override
	public void trade(Customer customer) throws JsonProcessingException {
		PMS100Event event = new PMS100Event(eventService, deliverService);
		event.copyProps(customer);
		event.setTradeChannel("POS");
		event.setMcc("7623");
		event.setRp01("0");
		ruleEngine.execute(event);
	}

	/**
	 * @param ruleEngine the ruleEngine to set
	 */
	public void setRuleEngine(RuleEngine ruleEngine) {
		this.ruleEngine = ruleEngine;
	}

	/**
	 * @param eventService the eventService to set
	 */
	public void setEventService(EventService eventService) {
		this.eventService = eventService;
	}

	/**
	 * @param deliverService the deliverService to set
	 */
	public void setDeliverService(DeliverService deliverService) {
		this.deliverService = deliverService;
	}

}
