/**
 * 
 */
package freedom.sample.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import freedom.sample.model.MarketActivity;
import freedom.sample.model.MarketChannel;
import freedom.sample.service.DeliverService;

/**
 * @author charlie
 *
 */
public class MutiDeliverServiceImpl implements DeliverService {
	
	private Map<DeliverChannel,BaseDeliverServiceImpl> map;
	
	public MutiDeliverServiceImpl() {
	}
	
	public MutiDeliverServiceImpl(BaseDeliverServiceImpl[] deliverServices) {
		map = new HashMap<DeliverChannel,BaseDeliverServiceImpl>();
		for (BaseDeliverServiceImpl deliverServiceBaseImpl : deliverServices) {
			map.put(deliverServiceBaseImpl.getDeliverChannel(), deliverServiceBaseImpl);
		}
	}

	/* (non-Javadoc)
	 * @see freedom.sample.service.DeliverService#deliver(freedom.sample.model.MarketingActivity)
	 */
	@Override
	public void deliver(MarketActivity marketingActivity) {
		List<MarketChannel> channels = marketingActivity.getChannels();
		for (MarketChannel marketChannel : channels) {
			DeliverService deliverService = map.get(marketChannel.getChannel());
			if(deliverService != null)
				try {
					deliverService.deliver(marketingActivity, marketChannel);
				} catch (Exception e) {
					e.printStackTrace();
				}
		}
	}

	/* (non-Javadoc)
	 * @see freedom.sample.service.DeliverService#deliver(freedom.sample.model.MarketActivity, freedom.sample.model.MarketChannel)
	 */
	@Override
	public void deliver(MarketActivity marketingActivity, MarketChannel marketChannel) {
		// TODO Auto-generated method stub
		
	}

}
