/**
 * 
 */
package freedom.sample.service.impl;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;

import freedom.sample.dao.OrganCustomParamDao;
import freedom.sample.dao.OrganRuleDao;
import freedom.sample.dao.RuleParamDao;
import freedom.sample.dao.RuleTemplateDao;
import freedom.sample.enums.RuleStatus;
import freedom.sample.model.OrganCustomParam;
import freedom.sample.model.OrganRule;
import freedom.sample.model.RuleInEngine;
import freedom.sample.model.RuleParam;
import freedom.sample.model.RuleTemplate;
import freedom.sample.service.RuleService;

/**
 * @author charlie
 *
 */
public class RuleServiceImpl implements RuleService {
	
	private RuleTemplateDao ruleTemplateDao;
	private RuleParamDao ruleParamDao;
	private OrganRuleDao organRuleDao;
	private OrganCustomParamDao organCustomParamDao;

	/* (non-Javadoc)
	 * @see freedom.sample.service.RuleService#findAllEnabledRuleTemplates()
	 */
	@Override
	public List<RuleTemplate> findAllEnabledRuleTemplates() {
		Object[] params = new Object[]{RuleStatus.ENABLED};
		String hql = "from RuleTemplate where status = ?0";
		return ruleTemplateDao.select(hql, params);
	}

	/* (non-Javadoc)
	 * @see freedom.sample.service.RuleService#findRuleParamsByRuleTemplateId(java.lang.Long[])
	 */
	@Override
	public List<RuleParam> findRuleParamsByRuleTemplateId(Collection<Long> ruleTemplateIds) {
		Object[] params = new Object[]{ruleTemplateIds};
		String hql = "from RuleParam where ruleTemplateId in ?0";
		return ruleParamDao.select(hql, params);
	}

	/* (non-Javadoc)
	 * @see freedom.sample.service.RuleService#findAllEnabledOrganRules()
	 */
	@Override
	public List<OrganRule> findAllEnabledOrganRules() {
		Object[] params = new Object[]{RuleStatus.ENABLED};
		String hql = "from OrganRule where status = ?0";
		return organRuleDao.select(hql, params);
	}

	/* (non-Javadoc)
	 * @see freedom.sample.service.RuleService#findAllOrganCustomParamsByRuleTemplateId(java.util.Collection)
	 */
	@Override
	public List<OrganCustomParam> findAllOrganCustomParamsByRuleTemplateId(Collection<Long> ruleTemplateIds) {
		Object[] params = new Object[]{ruleTemplateIds};
		String hql = "from OrganCustomParam where ruleTemplateId in ?0";
		return organCustomParamDao.select(hql, params);
	}

	/* (non-Javadoc)
	 * @see freedom.sample.service.RuleService#findAllChangedRuleTemplates(int)
	 */
	@Override
	public List<RuleTemplate> findAllChangedRuleTemplates(int days) {
		Object[] params = new Object[] { DateUtils.addDays(new Date(), days) };
		String hql = "from RuleTemplate where updateTime >= ?0";
		return ruleTemplateDao.select(hql, params);
	}

	/* (non-Javadoc)
	 * @see freedom.sample.service.RuleService#findAllChangedOrganRules(int)
	 */
	@Override
	public List<OrganRule> findAllChangedOrganRules(int days) {
		Object[] params = new Object[] { DateUtils.addDays(new Date(), days) };
		String hql = "from OrganRule where updateTime >= ?0";
		return organRuleDao.select(hql, params);
	}

	/* (non-Javadoc)
	 * @see freedom.sample.service.RuleService#saveRules(java.util.List)
	 */
	@Override
	public void saveRules(List<RuleInEngine> ruleInEngines) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @param ruleTemplateDao the ruleTemplateDao to set
	 */
	public void setRuleTemplateDao(RuleTemplateDao ruleTemplateDao) {
		this.ruleTemplateDao = ruleTemplateDao;
	}

	/**
	 * @param ruleParamDao the ruleParamDao to set
	 */
	public void setRuleParamDao(RuleParamDao ruleParamDao) {
		this.ruleParamDao = ruleParamDao;
	}

	/**
	 * @param organRuleDao the organRuleDao to set
	 */
	public void setOrganRuleDao(OrganRuleDao organRuleDao) {
		this.organRuleDao = organRuleDao;
	}

	/**
	 * @param organCustomParamDao the organCustomParamDao to set
	 */
	public void setOrganCustomParamDao(OrganCustomParamDao organCustomParamDao) {
		this.organCustomParamDao = organCustomParamDao;
	}


}
