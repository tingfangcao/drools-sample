/**
 * 
 */
package freedom.sample.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;

import freedom.sample.model.RuleParam;
import freedom.sample.model.RuleTemplate;

/**
 * @author charlie
 *
 */
public class TestRuleServiceImpl {

	public List<RuleTemplate> findAllEnabledRuleTemplates() {
		List<RuleTemplate> rts = new ArrayList<RuleTemplate>();
		String name =  ClassLoader.class.getResource("/freedom.sample/rule/template").getFile();
		File directory = new File(name);
		Iterator<File> it = FileUtils.iterateFiles(directory, new String[]{"drl"}, false);
		while(it.hasNext()) {
			File file = it.next();
			try {
				String text = FileUtils.readFileToString(file,Charset.defaultCharset());
				RuleTemplate rt = new RuleTemplate();
				file.getCanonicalPath();
				String fileName = file.getName();
				int index = fileName.lastIndexOf(".");
				String extension = fileName.substring(index + 1);
				String fileNameWithoutExtension = fileName.substring(0, index);
				rt.setId(Long.valueOf(fileNameWithoutExtension));
				
				rt.setRuleText(text);
				rts.add(rt);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return rts;
	}

	public List<RuleParam> findRuleParams() {
		List<RuleParam> params = new ArrayList<RuleParam>();
		/*params.add(new RuleParam(100,"999000","string","1",2));
		params.add(new RuleParam(100,"999000","string","POS",3));
		params.add(new RuleParam(100,"999000","string","7623,7629,7623,7629,8912,5713,5714,5718,5719,5211,5231,5712",4));
		params.add(new RuleParam(100,"999000","string","0",5));
		
		params.add(new RuleParam(100,"871000","string","1",2));
		params.add(new RuleParam(100,"871000","string","POS",3));
		params.add(new RuleParam(100,"871000","string","7623,7629",4));
		params.add(new RuleParam(100,"871000","string","0",5));
		*/
		return params;
	}


}
