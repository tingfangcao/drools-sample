/**
 * 
 */
package freedom.sample.service.impl;

import freedom.sample.model.MarketActivity;
import freedom.sample.model.MarketChannel;

/**
 * @author charlie
 *
 */
public class WechatDeliverServiceImpl extends BaseDeliverServiceImpl {

	/* (non-Javadoc)
	 * @see freedom.sample.service.impl.DeliverServiceBaseImpl#getDeliverChannel()
	 */
	@Override
	public DeliverChannel getDeliverChannel() {
		return DeliverChannel.wechat;
	}

	/* (non-Javadoc)
	 * @see freedom.sample.service.impl.DeliverServiceBaseImpl#deliver(freedom.sample.model.MarketingActivity)
	 */
	@Override
	public void deliver(MarketActivity marketingActivity) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see freedom.sample.service.DeliverService#deliver(freedom.sample.model.MarketActivity, freedom.sample.model.MarketChannel)
	 */
	@Override
	public void deliver(MarketActivity marketingActivity, MarketChannel marketChannel) {
		// TODO Auto-generated method stub
		
	}
	
	

}
