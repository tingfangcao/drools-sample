/**
 * 
 */
package freedom.sample.service.impl;

import freedom.sample.model.MarketActivity;
import freedom.sample.model.MarketChannel;

/**
 * @author charlie
 *
 */
public class PhoneDeliverServiceImpl extends BaseDeliverServiceImpl {
	
	/* (non-Javadoc)
	 * @see freedom.sample.service.impl.DeliverServiceBaseImpl#getDeliverChannel()
	 */
	@Override
	public DeliverChannel getDeliverChannel() {
		return DeliverChannel.phone;
	}
	
	/* (non-Javadoc)
	 * @see freedom.sample.service.DeliverService#deliver(freedom.sample.model.MarketActivity, freedom.sample.model.MarketChannel)
	 */
	@Override
	public void deliver(MarketActivity marketingActivity, MarketChannel marketChannel) {
		// TODO Auto-generated method stub
		System.out.println("交付营销渠道。客户：" + marketingActivity.getCustId() + "，其他：" + marketChannel);
	}

	/* (non-Javadoc)
	 * @see freedom.sample.service.DeliverService#deliver(freedom.sample.model.MarketActivity)
	 */
	@Override
	public void deliver(MarketActivity marketingActivity) {
		// TODO Auto-generated method stub
		
	}

}
