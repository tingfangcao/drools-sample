/**
 * 
 */
package freedom.sample.service.impl;

import freedom.sample.service.DeliverService;

/**
 * @author charlie
 *
 */
public abstract class BaseDeliverServiceImpl implements DeliverService {

	/**
	 * @return
	 */
	public abstract DeliverChannel getDeliverChannel();

}
