/**
 * 
 */
package freedom.sample.service;

import java.util.Set;

import freedom.sample.exception.UnsupportedException;
import freedom.sample.model.RuleInEngine;

/**
 * @author charlie
 *
 */
public interface RuleBuilder {
	
	public enum DataType {
		string,number,bool
	}
	
	/**
	 * 应用启动时初始化
	 * @return
	 * @throws UnsupportedException 
	 */
	public Set<RuleInEngine> buildRulesForEnginInit() throws UnsupportedException;

	/**
	 * 每天定时刷新
	 * @return
	 * @throws UnsupportedException 
	 */
	public Set<RuleInEngine> buildRulesForEnginRefresh() throws UnsupportedException;

}
