/**
 * 
 */
package freedom.sample.service;

import java.util.Collection;
import java.util.List;

import freedom.sample.model.OrganCustomParam;
import freedom.sample.model.OrganRule;
import freedom.sample.model.RuleInEngine;
import freedom.sample.model.RuleParam;
import freedom.sample.model.RuleTemplate;

/**
 * @author charlie
 *
 */
public interface RuleService {
	
	/**
	 * @return
	 */
	List<RuleTemplate> findAllEnabledRuleTemplates();
	
	/**
	 * @param ruleTemplateIds
	 * @return
	 */
	List<RuleParam> findRuleParamsByRuleTemplateId(Collection<Long> ruleTemplateIds);
	
	/**
	 * @return
	 */
	List<OrganRule> findAllEnabledOrganRules();
	
	/**
	 * @param ruleTemplateIds
	 * @return
	 */
	List<OrganCustomParam> findAllOrganCustomParamsByRuleTemplateId(Collection<Long> ruleTemplateIds);
	
	/**
	 * @param days
	 * @return
	 */
	List<RuleTemplate> findAllChangedRuleTemplates(int days);
	
	/**
	 * @param days
	 * @return
	 */
	List<OrganRule> findAllChangedOrganRules(int days);
	
	/**
	 * @param ruleInEngines
	 */
	void saveRules(List<RuleInEngine> ruleInEngines);

}
