/**
 * 
 */
package freedom.sample.service;

import com.fasterxml.jackson.core.JsonProcessingException;

import freedom.sample.model.Customer;

/**
 * @author charlie
 *
 */
public interface TradeService {
	
	void trade(Customer customer) throws JsonProcessingException;

}
