/**
 * 
 */
package freedom.sample.service;

import freedom.sample.model.MarketActivity;
import freedom.sample.model.MarketChannel;

/**
 * @author charlie
 *
 */
public interface DeliverService {
	
	public enum DeliverChannel {
		phone,wechat
	}
	
	void deliver(MarketActivity marketingActivity);

	/**
	 * @param marketingActivity
	 * @param marketChannel
	 */
	void deliver(MarketActivity marketingActivity, MarketChannel marketChannel);

}
