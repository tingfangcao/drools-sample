/**
 * 
 */
package freedom.sample.event;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * @author charlie
 *
 */
public interface PerfectExecutable {
	
	/**
	 * 事件被规则引擎执行前，保存现场
	 * @throws JsonProcessingException
	 */
	void record() throws JsonProcessingException;
	
	/**
	 * 事件被规则引擎执行，且命中时执行该逻辑。（需在规则文本文件中主动调用该方法）
	 */
	void handle();

}
