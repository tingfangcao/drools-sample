/**
 * 
 */
package freedom.sample.event;

import java.util.ArrayList;
import java.util.List;

import freedom.sample.model.MarketActivity;
import freedom.sample.model.MarketChannel;
import freedom.sample.service.DeliverService;
import freedom.sample.service.EventService;
import freedom.sample.service.DeliverService.DeliverChannel;

/**
 * @author charlie
 *
 */
public class PMS100Event extends BaseEvent {
	
	private String custId;//客户内码
	private String custOrgan;//客户法人机构
	private String custType;//客户类型
	
	private String tradeChannel;//交易渠道
	private String mcc;//商户类别码
	private String rp01;//是否有办理消费贷：0-否，1-是

	/**
	 * @param eventService
	 * @param deliverService
	 */
	public PMS100Event(EventService eventService, DeliverService deliverService) {
		super(eventService, deliverService);
	}

	@Override
	public void handle() {
		//TODO 命中时,通过相关渠道推介营销产品。同时，若记录了上下文,则可以关联此上下文
		MarketActivity marketingActivity = new MarketActivity();
		marketingActivity.setCustId(custId);
		List<MarketChannel> channels = new ArrayList<MarketChannel>();
		channels.add(new MarketChannel(DeliverChannel.phone, "您可能需要我们的产品：房产类消费贷，欢迎来询！"));
		marketingActivity.setChannels(channels);
		handle(marketingActivity);
	}


	/**
	 * @return the custId
	 */
	public String getCustId() {
		return custId;
	}


	/**
	 * @param custId the custId to set
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}


	/**
	 * @return the custOrgan
	 */
	public String getCustOrgan() {
		return custOrgan;
	}


	/**
	 * @param custOrgan the custOrgan to set
	 */
	public void setCustOrgan(String custOrgan) {
		this.custOrgan = custOrgan;
	}


	/**
	 * @return the custType
	 */
	public String getCustType() {
		return custType;
	}


	/**
	 * @param custType the custType to set
	 */
	public void setCustType(String custType) {
		this.custType = custType;
	}


	/**
	 * @return the tradeChannel
	 */
	public String getTradeChannel() {
		return tradeChannel;
	}


	/**
	 * @param tradeChannel the tradeChannel to set
	 */
	public void setTradeChannel(String tradeChannel) {
		this.tradeChannel = tradeChannel;
	}


	/**
	 * @return the mcc
	 */
	public String getMcc() {
		return mcc;
	}


	/**
	 * @param mcc the mcc to set
	 */
	public void setMcc(String mcc) {
		this.mcc = mcc;
	}


	/**
	 * @return the rp01
	 */
	public String getRp01() {
		return rp01;
	}


	/**
	 * @param rp01 the rp01 to set
	 */
	public void setRp01(String rp01) {
		this.rp01 = rp01;
	}

}
