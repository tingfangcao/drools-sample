/**
 * 
 */
package freedom.sample;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author charlie
 *
 */
public class TestRegex {
	
    private static void getStrings() {
        String str = "rrwerqq84461376qqasfdasdfrrwerqq84461377qqasfdasdaa654645aafrrwerqq84461378qqasfdaa654646aaasdfrrwerqq84461379qqasfdasdfrrwerqq84461376qqasfdasdf";
        Pattern p = Pattern.compile("qq(.*?)qq");
        Matcher m = p.matcher(str);
        ArrayList<String> strs = new ArrayList<String>();
        while (m.find()) {
            strs.add(m.group(1));            
        } 
        for (String s : strs){
            System.out.println(s);
        }        
    }
    
    private static void getStrings2() {
        String str = "2016年10月20日至2016年10月21日止";
        Pattern p = Pattern.compile("(\\d{4}年\\d{2}月\\d{2}日)");
        Matcher m = p.matcher(str);
        ArrayList<String> strs = new ArrayList<String>();
        while (m.find()) {
            strs.add(m.group(1));            
        } 
        ArrayList<String> newstrs = new ArrayList<String>();
        for (String s : strs){
            System.out.println(s);
            String pattern = "yyyy年MM月dd日";
            String pattern2 = "yyyy-MM-dd";
            SimpleDateFormat sdf2 = new SimpleDateFormat(pattern2);
            newstrs.add(sdf2.format(parse(s, pattern)));
        } 
        for (String s : newstrs){
        	System.out.println(s);
        }        
        
    }

	/**
	 * @param s
	 * @param pattern
	 * @return
	 */
	private static Date parse(String s, String pattern) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		Date date = null;
		try {
			date = sdf.parse(s);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}
    
    
    public static void main(String[] args) {
    	getStrings2();
	}

}
